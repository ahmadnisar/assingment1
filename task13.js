//  task 13

//  declaring variables
var num = 6;
var factors = []

//  function to get factors of a num
function NumFactors()
{
    if(num<1)
    {
        return "please enter a valid num"
    }

    for (i = 1; i <= Math.floor(Math.sqrt(num)); i += 1)
    {
        if (num % i === 0)
        {
            factors.push(i);
            if (num / i !== i)
            {
                factors.push(num / i);
            }
        }
    }
    return factors
}
console.log(NumFactors())
