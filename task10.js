//  task 9

//  declaring variables
var n = 2

//  function to check the datatype of argument
function IdentityMatrix()
{
    var arr = [] 
    var i
    var j

    for (i = 0; i < n; i++) 
    {
        arr[i] = [];
        for (j = 0; j < n; j++)
        {
            arr[i][j] = {i,j};
        }
    }
    return arr;
}
console.log(IdentityMatrix());