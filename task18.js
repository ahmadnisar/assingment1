
//  Iterative function to implement Binary Search 
var iterativeFunction = function (arr, x) { 
   
    //  declaring variables
    var start=0 
    var end=arr.length-1; 
          
    // Iterate while start not meets end 
    while (start<=end)
    { 
  
        // Find the mid index 
        let mid=Math.floor((start + end)/2); 

        if (arr[mid]===x)
        {
            return true;
        }  
        else if (arr[mid] < x)  
        {
            start = mid + 1; 
        }
             
        else
        {
            end = mid - 1; 
        }       
    } 

    return false; 
} 
   
// Driver code 
let arr = [1, 3, 5, 7, 8, 9]; 
let x = 5; 
   
if (iterativeFunction(arr, x, 0, arr.length-1)) 
{
    console.log("Element found"); 
}
else
{
    console.log("Element not found"); 
} 